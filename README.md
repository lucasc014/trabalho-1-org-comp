Integrantes: Lucas Carneiro de Souza - 11914601 //
             Matheus Odebrecht Oliveira - 11832670 //
             Uriel Ramiro Munerato - 11816060 //
             Victor Kenji Matsushita - 11816021 //
             
**Trabalho 1 - Jokenpô**

O jogo consiste em um simples jogo de pedra-papel-tesoura (ou Jokenpô) contra um sistema de Inteligência Artificial, mas que possuem os conceitos base da linguagem Assembly nele contidos.

A ideia base para a inspiração e construção deste trabalho foi fazer um jogo o qual todos já soubesse jogá-lo, mas que e ainda assim possuísse os conceitos fundamentais da linguagem Assembly. Ele consiste em escolher o que jogar (pedra, papel ou  tesoura) com um simples apertar de uma tecla, e com base no tempo de demora do usuário para apertar, a máquina também escolherá o que jogar, e assim o jogo seguirá.

A partir do momento que o usuário ou a máquina chegar a 5 pontos, o jogo vai terminar e o sistema irá declarar o campeão na tela :).

**Trabalho 2 - Submod**

O trabalho consiste em uma função, a qual recebe dois números inteiros e devolve o módulo da subtração entre os dois números. Em outras palavras, trata-se de uma função que mostra a diferença entre dois números inteiros por meio da linguagem Assembly.

O objetivo deste trabalho foi construir uma função relativamente simples, mas que mostrasse as operações matemáticas na linguagem Assembly e seu funcionamento :).
